import numpy as np
import matplotlib.pyplot as plt
import argparse
import os

plot_dir = "plots"

#
# Load data from file
#
def load_data(datafile_path, debug):
   array_load = np.loadtxt(datafile_path, dtype=[('version', 'U10'), ('nnodes', 'i4'), ('ntasks', 'i4'), ('cpus', 'i4'), ('nruns', 'i4'), ('perform', 'double'), ('dlb', 'U10'), ('npme', 'i4')])
   if debug:
      print("****************** Array begin ****************")
      print(array_load)
      print("****************** Array end ******************")
   return array_load

#
# Get unique data elements for each field (used for looping)
#
def unique(data, debug):
   unique_data = {}
   for name in data.dtype.names:
      unique_data[name] = np.unique(data[name])

   if debug:
      print("****************** Unique begin ***************")
      for item in unique_data.items():
         print(item)
      print("****************** Unique end *****************")

   return unique_data


#
# Create plot of nnodes vs performance, loop over tasks per node
#
def create_plot(name, data, version, dlb, ntasks_range):
   # Plot data
   #ntasks_range = [2, 4, 8, 16, 32, 36]
   for ntasks in ntasks_range:
      x = data[np.where((data["ntasks"] == ntasks) & (data["version"] == version) & (data["dlb"] == dlb))]["nnodes"]
      y = data[np.where((data["ntasks"] == ntasks) & (data["version"] == version) & (data["dlb"] == dlb))]["perform"]
      plt.plot(x, y, 'o-', label = "tasks " + str(ntasks), linewidth = 3, markersize = 10)
   
   # Setup plot
   plt.title(name)
   plt.xlabel('nnodes')
   plt.xticks(x)
   plt.ylabel('ns / day')
   plt.legend()
   
   # Save figure
   plt.savefig(os.path.join(plot_dir, name + ".png"))

   # clear figure
   plt.clf()

#
# Define main
#
def main():
   # Argument parser
   parser = argparse.ArgumentParser(formatter_class = argparse.ArgumentDefaultsHelpFormatter)
   parser.add_argument("data"         , type = str         , help = "Path to data file.")
   
   parser.add_argument("--debug", dest = "debug", help = "Debug run.", action  = "store_true")
   parser.set_defaults(debug = False)
   
   parser.add_argument("--name" , type = str , help = "Name of plot.", default = "plot")
   
   # Parse args
   args   = parser.parse_args()

   # Read in data
   data        = load_data(args.data, args.debug)
   unique_data = unique(data, args.debug)
   
   # Create plots
   for dlb in unique_data["dlb"]:
      for version in unique_data["version"]:
         plot_name = args.name + "_" + dlb + "_" + version

         create_plot(plot_name, data, version, dlb, unique_data["ntasks"])

   #create_plot("plot_no_2018.6" , data, "2018.6", "no")
   #create_plot("plot_yes_2018.6", data, "2018.6", "yes")
   #create_plot("plot_no_2019.4" , data, "2019.4", "no")
   #create_plot("plot_yes_2019.4", data, "2019.4", "yes")

# Call main
main()
