#!/bin/bash

mkdir -p plots

python plot.py --name mem_npme mem_npme/gather.data 
python plot.py --name mem mem/gather.data
python plot.py --name rib_npme rib_npme/gather.data 
python plot.py --name rib rib/gather.data
